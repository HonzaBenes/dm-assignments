from pulp import LpVariable, LpProblem, LpMaximize, LpStatus, value

c1 = LpVariable("c1", 0)
c2 = LpVariable("c2", 0)
c3 = LpVariable("c3", 0)
c4 = LpVariable("c4", 0)
c5 = LpVariable("c5", 0)
c6 = LpVariable("c6", 0)
c7 = LpVariable("c7", 0)
c8 = LpVariable("c8", 0)
c9 = LpVariable("c9", 0)
c10 = LpVariable("c10", 0)
c11 = LpVariable("c11", 0)
c12 = LpVariable("c12", 0)

# Additional variables


prob = LpProblem("problem", LpMaximize)

# First I will define the objective function
prob += 2000 * c1 + 2500 * c2 + 5000 * c3 + 3500 * c4 \
        + 2000 * c5 + 2500 * c6 + 5000 * c7 + 3500 * c8 \
        + 2000 * c9 + 2500 * c10 + 5000 * c11 + 3500 * c12

# Next I will add the constraints

# Weight capacity of first wagon
prob += c1 + c2 + c3 + c4 <= 10

# Weight capacity of second wagon
prob += c5 + c6 + c7 + c8 <= 8

# Weight capacity of third wagon
prob += c9 + c10 + c11 + c12 <= 12

# Volume capacity of first wagon
prob += 400 * c1 + 300 * c2 + 200 * c3 + 500 * c4 <= 5000
# Volume capacity of second wagon
prob += 400 * c5 + 300 * c6 + 200 * c7 + 500 * c8 <= 4000
# Volume capacity of third wagon
prob += 400 * c9 + 300 * c10 + 200 * c11 + 500 * c12 <= 8000

# Limit of available tons of the first cargo type
prob += c1 + c5 + c9 <= 18
# Limit of available tons of the second cargo type
prob += c2 + c6 + c10 <= 10
# Limit of available tons of the third cargo type
prob += c3 + c7 + c11 <= 5
# Limit of available tons of the fourth cargo type
prob += c4 + c8 + c12 <= 20

prob.writeLP("model.lp")

prob.solve()

print("Status:", LpStatus[prob.status])

for v in prob.variables():
    print(v.name, "=", v.varValue)

print("objective=", value(prob.objective))
print("\nSensitivity Analysis\nconstraint\t\tShadow Price\tSlack")

for name, c in prob.constraints.items():
    print(name, ":", c, "\t", c.pi, "\t\t", c.slack)
